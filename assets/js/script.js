/*
 *  SCRIPT.JS for Sloat Design
 * 	Author: Judah Nagler, 2014
 */


// On document ready:

$(document).ready(function() {

	// Vars
	var winW 		= $(window).width();
	var tablet      = 768;
	var phone       = 480;
	var isMobile  	= false;

	// Add dropdown hover
	function dropdownHover() {
		
		$('.dropdown').hover(function() {
	        if (!isMobile) $(this).addClass('open');
	    }, function() {
			if (!isMobile) $(this).removeClass('open');
	    });
	}

    // Resize 
    function resize() {
		winW = $(window).width();
		if (winW >= tablet) {
			isMobile = false;
			$('.dropdown').removeClass('open');
		} else {
			isMobile = true;
		}
		dropdownHover();
    }
	resize();

	// Call resize function on window event
	$(window).bind("resize", resize);
});